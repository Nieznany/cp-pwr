#include <cstdio>

using namespace std;

long long min3(long long x, long long y, long long z)
{
    if( x <= y && x <= z)
        return x;
    if( y <=x && y<= z)
        return y;
    return z;
}
int main()
{
    int T = 1;
    for(int t = 0;t < T; t++)
    {
        int n, m;
        scanf("%d", &n);
        scanf("%d", &m);

        long long** tab = new long long*[n];
        long long** tab2 = new long long*[n];
        for(int i = 0; i < n; i++)
        {
            tab[i]= new long long[m];
            tab2[i] = new long long[m];

            for(int j = 0; j < m; j++)
            {
                scanf("%lld", tab[i] + j);
                tab2[i][j] = tab[i][j];
            }
        }
        for(int j = 1; j < m; j++)
        {
            for (int i = 0; i < n; i++)
            {
                if (i == 0 && i == n-1)
                    tab2[i][j] = tab[i][j] + tab2[i][j-1];
                else if( i == 0)
                    tab2[i][j] = tab[i][j] + (tab2[i][j-1] < tab2[i+1][j-1] ? tab2[i][j-1] : tab2[i+1][j-1]);
                else if(i == n-1)
                    tab2[i][j] = tab[i][j] + (tab2[i][j-1] < tab2[i-1][j-1] ? tab2[i][j-1] : tab2[i-1][j-1]);
                else
                    tab2[i][j] = tab[i][j] + min3(tab2[i-1][j-1], tab2[i][j-1], tab2[i+1][j-1]);
            }

            for(int i = 1; i < n; i++)
            {
                tab2[i][j] = tab2[i][j] < tab[i][j] + tab2[i-1][j] ? tab2[i][j] : (tab[i][j] + tab2[i-1][j]);
            }
            for(int i = n-2; i >= 0; i--)
            {
                tab2[i][j] = tab2[i][j] < tab[i][j] + tab2[i+1][j] ? tab2[i][j] : (tab[i][j] + tab2[i+1][j]);

            }
        }
        long long min = tab2[0][m-1];
        for(int i = 1; i < n; i++)
            if(tab2[i][m-1] < min)
                min = tab2[i][m-1];
        printf("%lld\n", min);
    }
    return 0;
}
