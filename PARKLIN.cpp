#include <cstdio>
using namespace std;
int main()
{
    int n, m;
    scanf("%d", &n);
    scanf("%d", &m);
    int* kidsstart = new int[n];
    int* kidsend = new int[n];
    for(int i = 0; i < n; i++)
        scanf("%d", kidsstart + i);
    for(int i = 0; i < n; i++)
        scanf("%d", kidsend + i);
    int* translate = new int[n];
    for (int i = 0; i < n; i++)
        translate[kidsend[i]] = i;

    bool fail = false;
    for(int i = 0; i < n; ++i)
    {
        if (i - translate[kidsstart[i]] > m)
            fail = true;
    }
    if(fail)
        printf("NIE\n");
    else
        printf("TAK\n");
    return 0;
}
