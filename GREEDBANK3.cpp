#include <cstdio>
using namespace std;
int main()
{
    int n;
    scanf("%d", &n);
    int* nom = new int[n];
    for(int i = 0; i < n; ++i)
    {
        scanf("%d", &nom[i]);
    }
    int money;
    scanf("%d", &money);
    int c = 0;
    while (money > 0 && c < n)
    {
        if(nom[c] > money)
            c++;
        else
        {
            money -= nom[c];
            printf("%d ", nom[c]);
        }
    }
    if (money < 0)
        printf("klops...");
    printf("\n");
    return 0;
}

