#include <cstdio>

using namespace std;

int main()
{
    int n, k; 
    scanf("%d", &n);
    scanf("%d", &k);

    int* books = new int[n];
    for(int i = 0; i < n; i++)
        scanf("%d", books + i);
    int suma = 0;
    int res = 0;
    for(int i = 0; i < n; i++)
    {
        suma += books[i];
        if(suma > k)
        {
            suma = books[i];
            res += 1;
        }
    }
    printf("%d\n", res);
    return 0;
}
