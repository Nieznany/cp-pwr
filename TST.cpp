#include <cstdio>

int main()
{
    long long int t;
    scanf("%lld", &t);
    for (int i = 0; i < t; ++i){
        long long int n;
        scanf("%lld", &n);
        n = (n-1)%12;
        n = (n%3 + n/3)%4;
        switch(n){
        case 0:
            printf("A\n");
            break;
        case 1:
            printf("B\n");
            break;
        case 2:
            printf("C\n");
            break;
        case 3:
            printf("D\n");
            break;
        default:
            printf("BROKEN");
        }
    }
    return 0;
}
