#include <cstdio>
using namespace std;
int main()
{
    int n, k, r;
    scanf("%d %d %d", &n, &k, &r);
    int* kamery = new int[n];
    int kamera;
    for(int i = 0; i < k; i++)
    {
        scanf("%d", &kamera);
        kamery[kamera-1]=1;
    }

    int res = 0;
    int suma = 0;
    for(int i = 0; i < r; i++)
        suma += kamery[i];
    if(suma < 2)
    {
        if (suma < 1)
        {
            kamery[r-2]=1;
            kamery[r-1]=1;
            res += 2;
        }
        else if (kamery[r-1]==1)
        {
            kamery[r-2] = 1;
            res += 1;
        }
        else
        {
            res +=1;
            kamery[r-1] = 1;
        }
        suma = 2;
    }

    for(int i = 0; i < n - r; i++)
    {
        suma -= kamery[i];
        suma += kamery[r+i];
        if(suma < 2)
        {
            res += 1;
            kamery[r+i]=1;
            suma = 2;
        }
    }
    printf("%d\n", res);
    return 0;
}
