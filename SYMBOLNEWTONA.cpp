#include <cstdio>
int main()
{
    int n, k;
    scanf("%d %d", &n, &k);
    int* tablica = new int[n+1];
    tablica[0] = 1;
    for(int i = 0; i < n; i++)
    {
        tablica[i+1] = tablica[0];
        for(int j=i; j > 0;j--)
            tablica[j] = (tablica[j-1] + tablica[j])%10000;
    }
    printf("%d\n", tablica[k]);
    return 0;
}
