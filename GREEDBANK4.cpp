#include <cstdio>
using namespace std;
int main()
{
    int n;
    scanf("%d", &n);
    int* nom = new int[n];
    int* res = new int[n];
    for(int i = 0; i < n; ++i)
    {
        scanf("%d", &nom[i]);
        res[i] = 0;
    }
    int money;
    scanf("%d", &money);
    int c = 0;
    while (money > 0 && c < n)
    {
        if(nom[c] > money)
            c++;
        else
        {
            money -= nom[c];
            res[c] += 1;
        }
    }
    if( money > 0)
        printf("NIE");
    else
        for(int i = 0; i < n; i++)
            for(int j = 0; j < res[i]; j++)
                printf("%d ", nom[i]);
    printf("\n");
    return 0;
}

