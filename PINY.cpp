#include <cstdio>
using namespace std;
long long tablica[900];
long long piny(int n) {
    if (tablica[n] != -1)
        return tablica[n];
    tablica[n] =piny(n-1) + piny(n-2);
    return tablica[n];
}
int main() {
    tablica[0]=1;
    tablica[1]=1;
    for (int i = 2; i < 900; i++)
        tablica[i]=-1;
    int n;
    scanf("%d", &n);
    printf("%lld\n", piny(n));
    return 0;
}
