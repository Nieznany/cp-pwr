#include <string>
#include <iostream>
using namespace std;

bool check_ans(int m, int n, string s) {
    int i = 0;
    int num = 0;
    while (i < s.size())
    {
        if(s[i] == '+')
        {
            num += 1;
            i += m;
            if (num > n)
                return false;
        }
        else
            i +=1;
    }
    if (num > n)
        return false;
    return true;
}
bool check(string s)
{
    for (int i = 0; i < s.size(); i++)
        if(s[i] == '+')
            return false;
    return true;
}
int main()
{
    int n, m;
    string s;
    cin >> n;
    cin >> s;
    cin >> m;
    int min = 0;
    int max = n;
    int k;
    if(check(s)){
        cout << 0;
        return 0;
    }
    while (min + 1 < max)
    {
        k = (min + max)/2;
        if (!check_ans(k, m, s))
            min = k;
        else
            max = k;
    }
    cout << max;
    return 0;
}
