#include <cstdio>

using namespace std;

long long min3(long long x, long long y, long long z)
{
    if( x <= y && x <= z)
        return x;
    if( y <=x && y<= z)
        return y;
    return z;
}
int main()
{
    int T;
    scanf("%d", &T);
    for(int t = 0;t < T; t++)
    {
        int n, m;
        scanf("%d", &n);
        scanf("%d", &m);

        long long** tab = new long long*[n];
        for(int i = 0; i < n; i++)
        {
            tab[i]= new long long[m];

            for(int j = 0; j < m; j++)
            {
                scanf("%lld", tab[i] + j);
            }
        }
        for(int j = 1; j < m; j++)
        {
            for (int i = 0; i < n; i++)
            {
                if (i == 0 && i == n-1)
                    tab[i][j] += tab[i][j-1];
                else if( i == 0)
                    tab[i][j] += tab[i][j-1] < tab[i+1][j-1] ? tab[i][j-1] : tab[i+1][j-1];
                else if(i == n-1)
                    tab[i][j] += tab[i][j-1] < tab[i-1][j-1] ? tab[i][j-1] : tab[i-1][j-1];
                else
                    tab[i][j] += min3(tab[i-1][j-1], tab[i][j-1], tab[i+1][j-1]);
            }
        }
        long long min = tab[0][m-1];
        for(int i = 1; i < n; i++)
            if(tab[i][m-1] < min)
                min = tab[i][m-1];
        printf("%lld\n", min);
    }
    return 0;
}
