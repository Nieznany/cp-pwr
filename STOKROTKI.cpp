#include <cstdio>
using namespace std;

int main()
{
    int T;
    scanf("%d", &T);
    for(int t = 0; t < T; t++){
        int n, m;
        scanf("%d", &n);
        scanf("%d", &m);

        long long** tab = new  long long* [n];
        for(int i = 0; i < n; i++)
        {
            tab[i] = new long long[m];
            for(int j = 0; j < m; j++)
            {
                long long tmp;
                scanf("%lld", &tmp);
                if(i == 0 && j == 0)
                    tab[i][j] = tmp;
                else if(i == 0)
                    tab[i][j] = tmp + tab[i][j-1];
                else if(j == 0)
                    tab[i][j] = tmp + tab[i-1][j];
                else
                    tab[i][j] = tab[i-1][j] < tab[i][j-1] ? tab[i][j-1] + tmp : tab[i-1][j] + tmp;
            }
        }
        printf("%lld\n", tab[n-1][m-1]);
    }
    return 0;
}
