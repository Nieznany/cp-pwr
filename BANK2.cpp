#include <cstdio>
using namespace std;
int main()
{
    int n;
    scanf("%d", &n);

    int* money = new int[n];
    for(int i = 0; i < n; i++)
        scanf("%d", money + i);
    
    int k;
    scanf("%d", &k);

    int* wyniki = new int[k];
    for(int i = 0; i < k; i++)
    {
        wyniki[i] = -1;
        for(int j = 0; j < n; j++)
        {
            if(i - money[j] >= 0)
            {
                if (wyniki[i-money[j]] != -1 && (wyniki[i-money[j]] < wyniki[i] || wyniki[i] == -1))
                {
                    wyniki[i] = wyniki[i - money[j]] + 1;
                }
            }
            else if(i+1 - money[j] == 0)
            {
                wyniki[i] = 1;
            }
        }
    }
    printf("%d\n", wyniki[k-1]);
    return 0;
}
