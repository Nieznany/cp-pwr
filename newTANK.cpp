#include <cstdio>
using namespace std;

int main() {
    int T;
    scanf("%d", &T);
    for (int it = 0; it < T; ++it) {
        long int n, k;
        scanf("%ld", &n);
        scanf("%ld", &k);
        long int* tanks = new long int[n];
        for (long int i = 0; i < n; ++i)
            scanf("%ld", tanks + i);
        bool fail = false;
        for (long int i = 0; i < n; ++i)
            if (tanks[i] - i > k)
                fail = true;
        if(fail)
            printf("NIE\n");
        else
            printf("TAK\n");
    }
    return 0;
}

